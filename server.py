from flask import Flask, request, abort, jsonify
from pymongo import MongoClient
from bson import json_util, ObjectId
import json

PORT = 4242

app = Flask(__name__)

db_client = None
database = None
db_conf = None


def fix_json(j):

    def fix_array(info):
        for item in info:
            for key, value in item.items():
                if not isinstance(value, dict) or len(value) != 1:
                    continue
                (sub_key, sub_value), = value.items()
                if not sub_key.startswith('$'):
                    continue
                item[key] = sub_value

    j = json.dumps(j, sort_keys=True, indent=4, default=json_util.default)
    j = json.loads(j)
    a = [j]
    fix_array(a)
    return a[0]


def establish_db_connection():
    global db_client, database, db_conf
    with open('db_connection_conf.json') as f:
        db_conf = json.load(f)
        db_client = MongoClient(db_conf['IP'], int(db_conf['PORT']))
        database = db_client[db_conf['DATABASE']]


@app.route('/bots', methods=['POST'])
def add_bot():
    if not request.json or 'id' not in request.json or 'name' not in request.json:
        abort(400)
    req = database[db_conf['BOTS_COLLECTION']].find_one({'id': request.json})
    if req is not None:
        abort(400)
    database[db_conf['BOTS_COLLECTION']].insert_one(request.json.copy())
    return jsonify({'bot': request.json}), 201


@app.route('/bots/<bot_id>', methods=['GET'])
def get_bot(bot_id):
    req = database[db_conf['BOTS_COLLECTION']].find_one({'id': str(bot_id)})
    if '_id' in req:
        del req['_id']
    if req is None:
        abort(404)
    return jsonify(req), 200


@app.route('/bots/<bot_id>', methods=['DELETE'])
def delete_bot(bot_id):
    req = database[db_conf['BOTS_COLLECTION']].find_one({'id': str(bot_id)})
    if req is None:
        abort(404)
    database[db_conf['BOTS_COLLECTION']].delete_one({'id': str(bot_id)})
    return jsonify({'result': True}), 204


@app.route('/bots/<bot_id>', methods=['PUT'])
def update_bot(bot_id):
    if not request.json or 'id' not in request.json or 'name' not in request.json:
        abort(400)
    req = database[db_conf['BOTS_COLLECTION']].find_one({'id': bot_id})
    if req is None:
        abort(400)

    database[db_conf['BOTS_COLLECTION']].delete_one({'id': bot_id})
    database[db_conf['BOTS_COLLECTION']].insert_one(request.json.copy())
    return jsonify({'bot': request.json}), 201


@app.route('/messages', methods=['POST'])
def add_message():
    if not request.json or 'conversationId' not in request.json and\
            'timestamp' not in request.json and 'from' not in request.json and 'to' not in request.json:
        abort(400)
    to_add = request.json.copy()
    database[db_conf['MESSAGE_COLLECTION']].insert_one(to_add)
    return jsonify(fix_json(to_add)), 201


@app.route('/messages/<m_id>', methods=['GET'])
def get_message(m_id):
    req = database[db_conf['MESSAGE_COLLECTION']].find_one({'_id': ObjectId(m_id)})
    if req is None:
        abort(404)

    return jsonify(fix_json(req)), 200


@app.route('/messages', methods=['GET'])
def get_messages():
    param = request.args.get('conversationId')
    if param is None:
        abort(400)
    req = database[db_conf['MESSAGE_COLLECTION']].find({'conversationId': param})
    if req is None:
        abort(404)

    l = [fix_json(a) for a in req]
    return jsonify(l), 200


if __name__ == '__main__':
    establish_db_connection()
    app.run(debug=True, port=PORT)
